cmake_minimum_required(VERSION 3.5)

project(AhreedTest CXX)

set(CMAKE_CXX_STANDARD 11)

add_subdirectory(Ahreed)

add_executable(AhreedTest main.cpp)
target_link_libraries(AhreedTest ahreed)
