#include "Ahreed/Graphics/Window.h"
#include "Ahreed/Scene/ConsoleScene.h"
#include "Ahreed/Service/ServiceManager.h"

using namespace Ahreed;

int main() {
    Scene::ConsoleScene s;

    Graphics::Window win;

    Service::ServiceManager::provide(&win);
    Service::CallbackManager::registerCallbacks();

    win.setSize(1280, 720);
    win.setTitle("Hello, World!");

    win.setScene(&s, MAX_SCENES - 1);

    win.run();
    return 0;
}
